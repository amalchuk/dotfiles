#: Suppress the default login message:
set -g fish_greeting

#: Suppress shortening current working directory:
set -g fish_prompt_pwd_dir_length 0
