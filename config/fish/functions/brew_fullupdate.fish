function brew_fullupdate
    brew update --quiet --force; and brew outdated --quiet --greedy; and brew upgrade --greedy --no-quarantine; and brew cleanup --quiet --prune=all -s
    set -l homebrew_cache (brew --cache)
    rm -rf $homebrew_cache
end
