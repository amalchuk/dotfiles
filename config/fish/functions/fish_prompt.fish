function fish_prompt
    set -l last_status $status
    set -l status_color (set_color brred)
    test $last_status = 0; and set status_color (set_color normal)

    set -q VIRTUAL_ENV; and echo -ns (set_color white) '‹' (basename "$VIRTUAL_ENV") '›' (set_color normal) ' '

    echo -ns (set_color brcyan) $USER
    echo -ns (set_color brblack) '@'
    echo -ns (set_color brblue) (prompt_hostname)
    echo -ns (set_color brblack) ':'
    echo -ns (set_color normal) (prompt_pwd) ' '

    set -l git_branch (git --no-optional-locks branch --show-current 2> /dev/null)
    if test -n "$git_branch"
        set -l git_status (git --no-optional-locks status --porcelain 2> /dev/null | tail -n 1)

        set -l git_branch_color (set_color brgreen)
        test -n "$git_status"; and set git_branch_color (set_color bryellow)

        echo -ns $git_branch_color '‹' $git_branch '›' (set_color normal)
    end

    echo
    echo -ns $status_color '❯' (set_color normal) ' '
end
