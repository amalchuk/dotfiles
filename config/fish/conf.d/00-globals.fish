#: Default locale:
set -gx LANGUAGE "en_US.UTF-8"
set -gx LC_ALL "en_US.UTF-8"
set -gx LANG "en_US.UTF-8"

#: Default text editor:
set -gx EDITOR "vim"
set -gx VISUAL "vim"

#: Git environment variables:
set -gx GIT_CONFIG_NOSYSTEM "true"

#: Python environment variables:
set -gx PYTHONDONTWRITEBYTECODE "true"
set -gx VIRTUAL_ENV_DISABLE_PROMPT "true"

#: Suppress saving a history file:
set -gx LESSHISTFILE "/dev/null"

#: Default directories for the man pages:
set -q MANPATH; or set -gxp --path MANPATH "/usr/share/man" "/usr/local/share/man"
