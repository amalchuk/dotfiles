#: Initial setup:
set -gx HOMEBREW_PREFIX "/opt/homebrew"
set -gx HOMEBREW_CELLAR "/opt/homebrew/Cellar"
set -gx HOMEBREW_REPOSITORY "/opt/homebrew"

set -gxp --path PATH "/opt/homebrew/bin" "/opt/homebrew/sbin"
set -gxp --path MANPATH "/opt/homebrew/share/man"
set -gxp --path INFOPATH "/opt/homebrew/share/info"

#: Disable some annoyable things:
set -gx HOMEBREW_NO_ANALYTICS "true"
set -gx HOMEBREW_NO_AUTO_UPDATE "true"
set -gx HOMEBREW_NO_EMOJI "true"

#: [brew install --formula binutils]
#: GNU binary tools for native development:
if test -d "$HOMEBREW_PREFIX/opt/binutils"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/binutils/bin"
end

#: [brew install --formula coreutils]
#: GNU File, Shell and Text utilities:
if test -d "$HOMEBREW_PREFIX/opt/coreutils"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/coreutils/libexec/gnubin"
end

#: [brew install --formula curl]
#: HTTP/HTTPS/FTP client:
if test -d "$HOMEBREW_PREFIX/opt/curl"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/curl/bin"
end

#: [brew install --formula ed]
#: Classic UNIX line editor:
if test -d "$HOMEBREW_PREFIX/opt/ed"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/ed/libexec/gnubin"
end

#: [brew install --formula findutils]
#: Collection of GNU find, xargs and locate:
if test -d "$HOMEBREW_PREFIX/opt/findutils"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/findutils/libexec/gnubin"
end

#: [brew install --formula gawk]
#: GNU awk utility:
if test -d "$HOMEBREW_PREFIX/opt/gawk"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/gawk/libexec/gnubin"
end

#: [brew install --formula gnu-getopt]
#: Command-line option parsing utility:
if test -d "$HOMEBREW_PREFIX/opt/gnu-getopt"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/gnu-getopt/bin"
end

#: [brew install --formula gnu-indent]
#: C code prettifier:
if test -d "$HOMEBREW_PREFIX/opt/gnu-indent"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/gnu-indent/libexec/gnubin"
end

#: [brew install --formula gnu-sed]
#: GNU implementation of the famous stream editor:
if test -d "$HOMEBREW_PREFIX/opt/gnu-sed"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/gnu-sed/libexec/gnubin"
end

#: [brew install --formula gnu-tar]
#: GNU version of the tar archiving utility:
if test -d "$HOMEBREW_PREFIX/opt/gnu-tar"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/gnu-tar/libexec/gnubin"
end

#: [brew install --formula gnu-time]
#: GNU implementation of time utility:
if test -d "$HOMEBREW_PREFIX/opt/gnu-time"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/gnu-time/libexec/gnubin"
end

#: [brew install --formula gnu-which]
#: GNU implementation of which utility:
if test -d "$HOMEBREW_PREFIX/opt/gnu-which"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/gnu-which/libexec/gnubin"
end

#: [brew install --formula grep]
#: GNU grep, egrep and fgrep:
if test -d "$HOMEBREW_PREFIX/opt/grep"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/grep/libexec/gnubin"
end

#: [brew install --formula openjdk]
#: OpenJDK environment:
if test -d "$HOMEBREW_PREFIX/opt/openjdk"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/openjdk/bin"
end

#: [brew install --formula libpq]
#: PostgreSQL C API library:
if test -d "$HOMEBREW_PREFIX/opt/libpq"
    set -gxp --path PATH "$HOMEBREW_PREFIX/opt/libpq/bin"
end
