#: Configure the shell environment for Golang:
if command -q go
    set -gx GOPATH "$HOME/.golang"
    set -gxp --path PATH "$GOPATH/bin"
end

#: Configure the shell environment for Python:
if command -q pyenv
    status is-login; and pyenv init --path | source
    status is-interactive; and pyenv init - | source
end

#: Configure the shell environment for Rust:
if test -d "$HOME/.cargo"
    set -gxp --path PATH "$HOME/.cargo/bin"
end

#: Locally installed binaries:
if test -d "$HOME/.local/bin"
    set -gxp --path PATH "$HOME/.local/bin"
end
