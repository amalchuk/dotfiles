use framework "Cocoa"
set sourcePath to "extras/alacritty.icns"
set destPath to "/Applications/Alacritty.app"
set imageData to (current application's NSImage's alloc()'s initWithContentsOfFile:sourcePath)
(current application's NSWorkspace's sharedWorkspace()'s setIcon:imageData forFile:destPath options:2)
