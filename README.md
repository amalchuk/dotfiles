# dotfiles

Your dotfiles are how you personalize your system. These are mine 😊

## Installation

```
$ sh -c "$(curl -fsSL https://gitlab.com/amalchuk/dotfiles/-/raw/master/install)"
```

## Distribution

This project is licensed under the terms of the [MIT No Attribution](LICENSE).
