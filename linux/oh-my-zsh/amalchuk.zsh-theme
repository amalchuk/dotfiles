#: Enables substitutions:
setopt PROMPT_SUBST

amalchuk_get_prompt() {
    if [[ -v VIRTUAL_ENV ]]; then
        echo -n "%F{7}‹"$(basename "$VIRTUAL_ENV")"›%f "
    fi

    echo -n "%F{14}%n"
    echo -n "%F{8}@"
    echo -n "%F{12}%m"
    echo -n "%F{8}:"
    echo -n "%f%~"
    echo -n " "

    local git_branch=$(git --no-optional-locks branch --show-current 2> /dev/null)
    if [[ -n "$git_branch" ]]; then
        local git_status=$(git --no-optional-locks status --porcelain 2> /dev/null | tail -n 1)
        [[ -n "$git_status" ]] && echo -n "%F{11}" || echo -n "%F{10}"
        echo -n "‹${git_branch}›%f"
    fi

    echo
    echo -n "%(?.%f.%F{9})❯%f "
}

export PROMPT='$(amalchuk_get_prompt)'
