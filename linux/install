#!/bin/sh

#: Exit immediately if a pipeline returns a non-zero status:
set -eux

#: Temporary working directory:
DOTFILES_TMP=$(mktemp -d)

#: Clone the repository:
git clone --depth=1 --branch=master https://gitlab.com/amalchuk/dotfiles.git $DOTFILES_TMP

#: Dotfiles installation:
install -d -m 0700 ~/.dotfiles

install -m 0600 $DOTFILES_TMP/dotfiles/gitconfig ~/.dotfiles/.gitconfig
install -m 0600 $DOTFILES_TMP/dotfiles/gitignore ~/.dotfiles/.gitignore
install -m 0600 $DOTFILES_TMP/dotfiles/npmrc ~/.dotfiles/.npmrc
install -m 0600 $DOTFILES_TMP/dotfiles/vimrc ~/.dotfiles/.vimrc
install -m 0600 $DOTFILES_TMP/linux/aliases ~/.dotfiles/.aliases
install -m 0600 $DOTFILES_TMP/linux/provisioning ~/.dotfiles/.provisioning
install -m 0600 $DOTFILES_TMP/linux/zprofile ~/.dotfiles/.zprofile
install -m 0600 $DOTFILES_TMP/linux/zshenv ~/.dotfiles/.zshenv
install -m 0600 $DOTFILES_TMP/linux/zshrc ~/.dotfiles/.zshrc

#: Custom oh-my-zsh theme:
install -m 0600 $DOTFILES_TMP/linux/oh-my-zsh/amalchuk.zsh-theme ~/.oh-my-zsh/custom/themes/amalchuk.zsh-theme

ln -sf ~/.dotfiles/.gitconfig ~/.gitconfig
ln -sf ~/.dotfiles/.gitignore ~/.gitignore
ln -sf ~/.dotfiles/.npmrc ~/.npmrc
ln -sf ~/.dotfiles/.vimrc ~/.vimrc
ln -sf ~/.dotfiles/.zprofile ~/.zprofile
ln -sf ~/.dotfiles/.zshenv ~/.zshenv
ln -sf ~/.dotfiles/.zshrc ~/.zshrc

#: Configurations installation:
install -d -m 0700 ~/.config
install -d -m 0700 ~/.config/neofetch
install -d -m 0700 ~/.config/tmux

install -m 0600 $DOTFILES_TMP/config/neofetch/bongo.txt ~/.config/neofetch/bongo.txt
install -m 0600 $DOTFILES_TMP/config/neofetch/config.conf ~/.config/neofetch/config.conf
install -m 0600 $DOTFILES_TMP/config/tmux/tmux.conf ~/.config/tmux/tmux.conf

#: Cleanup:
rm -rf $DOTFILES_TMP
